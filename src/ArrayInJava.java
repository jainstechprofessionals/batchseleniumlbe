
public class ArrayInJava {

	public static void main(String[] args) {

		int arr[] = new int[5];

		int temp = 5;
		for (int i = 0; i < 5; i++) {
			arr[i] = temp;
			temp++;
		} // arr[0]=0

		for (int j = 0; j < arr.length; j++) {
			System.out.println(arr[j]);
		}

		int a[] = { 1, 2, 3, 4, 5 }; // a[0],a[1],a[2],a[3],a[4]

		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i]);
		}

	}

}
