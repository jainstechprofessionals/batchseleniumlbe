
public class SwitchProgram {

	public static void main(String[] args) {
		int i = 1;

		switch (i) {

		case 1:
			System.out.println(10 * 20);
			break;

		case 2:

			System.out.println(10 + 20);
			break;
		case 3:
			System.out.println(100 - 20);
			break;

		default:
			System.out.println("There is no case");
		}

	}
}
